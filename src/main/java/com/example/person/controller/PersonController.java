package com.example.person.controller;

import com.example.common.topics.Person;
import com.example.person.dao.PersonDao;
import com.example.person.producer.PersonProducer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.stream.Collectors;

import static com.example.common.domain.Record.CrudType.*;

@Slf4j
@Component
@RequiredArgsConstructor
public class PersonController {

    private final PersonProducer producer;

    private final PersonDao fileDao;

    public Mono<ServerResponse> findAll(ServerRequest request) {
        return Mono
                .from(fileDao
                        .findAll()
                        .collect(Collectors.toList()))
                .flatMap(persons ->
                        ServerResponse
                                .status(HttpStatus.OK)
                                .bodyValue(persons))
                .onErrorResume(e ->
                        ServerResponse
                                .badRequest()
                                .bodyValue(e.getMessage()));
    }

    public Mono<ServerResponse> create(ServerRequest request) {
        return producer
                .send(CREATE, request
                        .bodyToMono(Person.class)
                        .doOnNext(person -> log.info("creating person: " + person)))
                .flatMap(o ->
                        ServerResponse
                                .status(HttpStatus.CREATED)
                                .build())
                .onErrorResume(e ->
                        ServerResponse
                                .badRequest()
                                .bodyValue(e.getMessage()));
    }

    public Mono<ServerResponse> update(ServerRequest request) {
        return producer
                .send(UPDATE, request
                        .bodyToMono(Person.class)
                        .doOnNext(person -> log.info("updating person: " + person)))
                .flatMap(unused ->
                        ServerResponse
                                .status(HttpStatus.OK)
                                .build())
                .onErrorResume(error ->
                        ServerResponse
                                .badRequest()
                                .bodyValue(error.getMessage()));
    }

    public Mono<ServerResponse> remove(ServerRequest request) {
        return producer
                .send(DELETE, request
                        .queryParam("id")
                        .map(id -> Person.builder().id(id).build())
                        .map(Mono::just)
                        .orElseGet(() -> Mono.error(new IllegalArgumentException("ID is required"))))
                .flatMap(unused ->
                        ServerResponse
                                .status(HttpStatus.ACCEPTED)
                                .build())
                .onErrorResume(error ->
                        ServerResponse
                                .badRequest()
                                .bodyValue(error.getMessage()));
    }
}
