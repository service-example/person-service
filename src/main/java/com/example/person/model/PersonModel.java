package com.example.person.model;

import com.example.common.topics.Person;
import lombok.*;
import org.springframework.data.annotation.Id;

@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class PersonModel {

    @Id
    private String id;

    private String name;

    public static PersonModel from(Person person) {
        return PersonModel.builder()
                .id(person.getId())
                .name(person.getName())
                .build();
    }
}
