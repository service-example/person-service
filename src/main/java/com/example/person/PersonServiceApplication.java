package com.example.person;

import com.example.common.config.ConfigProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

@Slf4j
@SpringBootApplication
@EnableEurekaClient
@ComponentScan(basePackages = "com.example")
public class PersonServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonServiceApplication.class, args);
    }

    @Bean
    public Docket api(ConfigProperties config) {
        ApiInfo apiInfo = new ApiInfo(
                config.getServiceName(),
                "",
                "1.0.0",
                "",
                new Contact("Armin Butkereit", "", "abutkereit@pm.me"),
                "",
                "",
                Collections.emptyList());

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.person.controller"))
                .paths(PathSelectors.any())
                .build();
    }
}
