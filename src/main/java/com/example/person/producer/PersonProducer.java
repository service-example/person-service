package com.example.person.producer;

import com.example.common.domain.Record;
import com.example.common.domain.Record.CrudType;
import com.example.common.topics.Person;
import com.example.person.dao.PersonDao;
import com.example.person.model.PersonModel;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.NotImplementedException;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.kafka.sender.KafkaSender;

@Service
@RequiredArgsConstructor
public class PersonProducer {

    private final KafkaSender<String, Record<Person>> sender;

    private final PersonDao personDao;

    public Mono<Void> send(CrudType type, Mono<Person> person) {
        return person
                .flatMap(p ->
                        switch (type) {
                            case CREATE -> personDao
                                    .save(PersonModel.from(p))
                                    .thenReturn(p);
                            case UPDATE -> personDao
                                    .update(p.getId(), PersonModel.from(p))
                                    .thenReturn(p);
                            case DELETE -> personDao
                                    .deleteById(p.getId())
                                    .thenReturn(Person.builder()
                                            .id(p.getId())
                                            .build());
                            default -> Mono.error(new NotImplementedException("No Read yet :("));
                        })
                .flatMap(f ->
                        sender.createOutbound()
                                .send(Flux.just(
                                        new ProducerRecord<>("person",
                                                Record.<Person>builder()
                                                        .topic("person")
                                                        .type(type)
                                                        .data(f)
                                                        .build())))
                                .then());
    }
}
