package com.example.person.config.kafka;

import com.example.common.domain.Record;
import com.example.common.topics.Person;
import com.example.service.utils.kafka.JsonValueSerializer;
import com.fasterxml.jackson.databind.type.TypeFactory;

public class PersonRecordSerializer extends JsonValueSerializer<Record<Person>> {
    @Override
    protected Class<Record<Person>> getType() {
        return TypeFactory
                .defaultInstance()
                .constructParametricType(Record.class, Person.class)
                .getTypeHandler();
    }
}