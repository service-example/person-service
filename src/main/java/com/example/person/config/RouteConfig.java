package com.example.person.config;

import com.example.person.controller.PersonController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;

@Configuration(proxyBeanMethods = false)
public class RouteConfig {

    @Bean
    public RouterFunction<ServerResponse> route(PersonController personController) {
        return RouterFunctions
                .route(
                        GET("/person"), personController::findAll)
                .andRoute(
                        POST("/person").and(accept(MediaType.APPLICATION_JSON)), personController::create)
                .andRoute(
                        PUT("/person").and(accept(MediaType.APPLICATION_JSON)), personController::update)
                .andRoute(
                        DELETE("/person"), personController::remove);
    }
}
