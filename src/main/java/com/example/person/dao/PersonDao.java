package com.example.person.dao;

import com.example.person.model.PersonModel;
import com.example.person.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class PersonDao {
    private final PersonRepository personRepository;

    public Mono<PersonModel> save(PersonModel personModel) {
        return personRepository.insert(personModel.getId(), personModel.getName());
    }

    public Flux<PersonModel> findAll() {
        return personRepository.findAll();
    }

    public Mono<Void> deleteById(String id) {
        return personRepository.deleteById(id);
    }

    public Mono<PersonModel> update(String id, PersonModel personModel) {
        return personRepository
                .findById(id)
                .flatMap(value ->
                        Mono.just(value
                                .toBuilder()
                                .name(personModel.getName())
                                .build()))
                .flatMap(personRepository::save);
    }

}
