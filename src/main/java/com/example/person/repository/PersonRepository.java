package com.example.person.repository;

import com.example.person.model.PersonModel;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface PersonRepository extends ReactiveCrudRepository<PersonModel, String> {

    @Query("INSERT INTO public.person_model(id, name) VALUES (:id, :name)")
    Mono<PersonModel> insert(String id, String name);

}
